from openerp.osv import fields, osv
from openerp import SUPERUSER_ID


_ID_GROUP_SKP_ATASAN = 23
class res_users(osv.Model):
    _inherit = 'res.users'

    def write(self, cr, uid, ids, vals, context=None):
        for user_obj in self.browse(cr,uid,ids,context=None):
            if vals.get('email', False):
                vals['mail'] = vals.get('email', False)
            if user_obj.partner_id :
                if not vals.get('email', False):
                    vals['mail'] = user_obj.partner_id.email
                vals['mobile'] = user_obj.partner_id.mobile
                if user_obj.partner_id.job_id:
                    vals['jabatan'] = user_obj.partner_id.job_id.name
            if user_obj.company_id:
                vals['company_name'] = user_obj.company_id.name
        return super(res_users, self).write(cr, uid, ids, vals, context=context)

    _columns = {
        'mail': fields.char('Email', size=50, ),
        'mobile'    : fields.char('Mobile', size=50, ),
        'company_name': fields.char('Company Name', size=200, ),
        'jabatan': fields.char('Jabatan', size=200, ),
    }

    # @api.multi
    # def _get_total_kegiatan_pegawai_by_verifikatur(self):
    #     now = DateTime.today();
    #     last_month = now + DateTime.RelativeDateTime(months=-1)
    #     p_month = str(last_month.month).zfill(2)
    #     p_year = str(last_month.year)
    #     for obj in self:
    #         total = [0]
    #         if obj.partner_id and not obj.partner_id.employee:
    #             domain = obj.id, p_month, p_year
    #             query = ''' select count(p.id)
    #                                 from project_skp p
    #                                 where
    #                                     p.user_id_bkd = %s
    #                                     and p.target_period_month= %s
    #                                     and p.target_period_year = %s
    #                                     and p.state in ('realisasi','rejected_manager')
    #                                     and p.active
    #                              '''
    #             self.env.cr.execute(query, domain)
    #             total = self.env.cr.fetchone() or [0]  # this method returned array
    #         obj.total_kegiatan_pegawai_verifikatur = total[0]
    #
    # @api.multi
    # def _get_total_kegiatan_atasan_by_verifikatur(self):
    #     now = DateTime.today();
    #     last_month = now + DateTime.RelativeDateTime(months=-1)
    #     p_month = str(last_month.month).zfill(2)
    #     p_year = str(last_month.year)
    #     for obj in self:
    #         total = [0]
    #         if obj.partner_id and not obj.partner_id.employee:
    #             domain = obj.id, p_month, p_year
    #             query = ''' select count(p.id)
    #                                     from project_skp p
    #                                     where
    #                                         p.user_id_bkd = %s
    #                                         and p.target_period_month= %s
    #                                         and p.target_period_year = %s
    #                                         and p.state in ('propose','appeal','rejected_bkd')
    #                                         and p.active
    #                                  '''
    #             self.env.cr.execute(query, domain)
    #             total = self.env.cr.fetchone() or [0]  # this method returned array
    #         obj.total_kegiatan_atasan_verifikatur = total[0]
    #
    # @api.multi
    # def _get_total_kegiatan_verifikatur_by_verifikatur(self):
    #     now = DateTime.today();
    #     last_month = now + DateTime.RelativeDateTime(months=-1)
    #     p_month = str(last_month.month).zfill(2)
    #     p_year = str(last_month.year)
    #     for obj in self:
    #         total = [0]
    #         if obj.partner_id and not obj.partner_id.employee:
    #             domain = obj.id, p_month, p_year
    #             query = ''' select count(p.id)
    #                                         from project_skp p
    #                                         where
    #                                             p.user_id_bkd = %s
    #                                             and p.target_period_month= %s
    #                                             and p.target_period_year = %s
    #                                             and p.state in ('evaluated','propose_to_close','propose_to_review')
    #                                             and p.active
    #                                      '''
    #             self.env.cr.execute(query, domain)
    #             total = self.env.cr.fetchone() or [0]  # this method returned array
    #         obj.total_kegiatan_verifikatur_verifikatur = total[0]
    #
    # @api.multi
    # def _get_total_kegiatan_selesai_by_verifikatur(self):
    #     now = DateTime.today();
    #     last_month = now + DateTime.RelativeDateTime(months=-1)
    #     p_month = str(last_month.month).zfill(2)
    #     p_year = str(last_month.year)
    #     for obj in self:
    #         total = [0]
    #         if obj.partner_id and not obj.partner_id.employee:
    #             domain = obj.id, p_month, p_year
    #             query = ''' select count(p.id)
    #                                             from project_skp p
    #                                             where
    #                                                 p.user_id_bkd = %s
    #                                                 and p.target_period_month= %s
    #                                                 and p.target_period_year = %s
    #                                                 and p.state = 'done'
    #                                                 and p.active
    #                                          '''
    #             self.env.cr.execute(query, domain)
    #             total = self.env.cr.fetchone() or [0]  # this method returned array
    #         obj.total_kegiatan_selesai_verifikatur = total[0]
    #
    #
    # #api v7-v8
    #
    # @api.v7
    # def api_get_total_kegiatan_pegawai(self, cr, uid, ids, context=None):
    #     user_id = len(ids) and ids[0] or False
    #     nilai = 0.0
    #     now = DateTime.today();
    #     last_month = now + DateTime.RelativeDateTime(months=-1)
    #     p_month = str(last_month.month).zfill(2)
    #     p_year = str(last_month.year)
    #     if user_id:
    #         if p_month and p_year:
    #             domain = user_id, p_month, p_year
    #             query = ''' select count(p.id)
    #                                 from project_skp p
    #                                 where
    #                                     p.user_id_atasan = %s
    #                                     and p.target_period_month= %s
    #                                     and p.target_period_year = %s
    #                                     and p.state in ('realisasi','rejected_manager')
    #                                     and p.active
    #                              '''
    #             cr.execute(query, domain)
    #             total = cr.fetchone() or [0]  # this method returned array
    #             nilai = total[0]
    #     return nilai
    #
    # @api.v7
    # def api_get_total_kegiatan_atasan(self, cr, uid, ids, context=None):
    #     user_id = len(ids) and ids[0] or False
    #     nilai = 0.0
    #     now = DateTime.today();
    #     last_month = now + DateTime.RelativeDateTime(months=-1)
    #     p_month = str(last_month.month).zfill(2)
    #     p_year = str(last_month.year)
    #     if user_id:
    #         if p_month and p_year:
    #             domain = user_id, p_month, p_year
    #             query = ''' select count(p.id)
    #                                     from project_skp p
    #                                     where
    #                                         p.user_id_atasan = %s
    #                                         and p.target_period_month= %s
    #                                         and p.target_period_year = %s
    #                                         and p.state in ('propose','appeal','rejected_bkd')
    #                                         and p.active
    #                                  '''
    #             cr.execute(query, domain)
    #             total = cr.fetchone() or [0]  # this method returned array
    #             nilai = total[0]
    #     return nilai
    #
    # @api.v7
    # def api_get_total_kegiatan_verifikatur(self, cr, uid, ids, context=None):
    #     user_id = len(ids) and ids[0] or False
    #     nilai = 0.0
    #     now = DateTime.today();
    #     last_month = now + DateTime.RelativeDateTime(months=-1)
    #     p_month = str(last_month.month).zfill(2)
    #     p_year = str(last_month.year)
    #     if user_id:
    #         if p_month and p_year:
    #             domain = user_id, p_month, p_year
    #             query = ''' select count(p.id)
    #                                         from project_skp p
    #                                         where
    #                                             p.user_id_atasan = %s
    #                                             and p.target_period_month= %s
    #                                             and p.target_period_year = %s
    #                                             and p.state in ('evaluated','propose_to_close','propose_to_review')
    #                                             and p.active
    #                                      '''
    #             cr.execute(query, domain)
    #             total = cr.fetchone() or [0]  # this method returned array
    #             nilai = total[0]
    #     return nilai
    #
    # @api.v7
    # def api_get_total_kegiatan_selesai(self, cr, uid, ids, context=None):
    #     user_id = len(ids) and ids[0] or False
    #     nilai = 0.0
    #     now = DateTime.today();
    #     last_month = now + DateTime.RelativeDateTime(months=-1)
    #     p_month = str(last_month.month).zfill(2)
    #     p_year = str(last_month.year)
    #     if user_id:
    #         if p_month and p_year:
    #             domain = user_id, p_month, p_year
    #             query = ''' select count(p.id)
    #                                             from project_skp p
    #                                             where
    #                                                 p.user_id_atasan = %s
    #                                                 and p.target_period_month= %s
    #                                                 and p.target_period_year = %s
    #                                                 and p.state = 'done'
    #                                                 and p.active
    #                                          '''
    #             cr.execute(query, domain)
    #             total = cr.fetchone() or [0]  # this method returned array
    #             nilai = total[0]
    #     return nilai
    #
    # @api.v7
    # def api_get_total_kegiatan_pegawai_by_verifikatur(self, cr, uid, ids, context=None):
    #     user_id = len(ids) and ids[0] or False
    #     nilai = 0.0
    #     now = DateTime.today();
    #     last_month = now + DateTime.RelativeDateTime(months=-1)
    #     p_month = str(last_month.month).zfill(2)
    #     p_year = str(last_month.year)
    #     if user_id:
    #         if p_month and p_year:
    #             domain = user_id, p_month, p_year
    #             query = ''' select count(p.id)
    #                                     from project_skp p
    #                                     where
    #                                         p.user_id_bkd = %s
    #                                         and p.target_period_month= %s
    #                                         and p.target_period_year = %s
    #                                         and p.state in ('realisasi','rejected_manager')
    #                                         and p.active
    #                                  '''
    #             cr.execute(query, domain)
    #             total = cr.fetchone() or [0]  # this method returned array
    #             nilai = total[0]
    #     return nilai
    #
    # @api.v7
    # def api_get_total_kegiatan_atasan_by_verifikatur(self, cr, uid, ids, context=None):
    #     user_id = len(ids) and ids[0] or False
    #     nilai = 0.0
    #     now = DateTime.today();
    #     last_month = now + DateTime.RelativeDateTime(months=-1)
    #     p_month = str(last_month.month).zfill(2)
    #     p_year = str(last_month.year)
    #     if user_id:
    #         if p_month and p_year:
    #             domain = user_id, p_month, p_year
    #             query = ''' select count(p.id)
    #                                         from project_skp p
    #                                         where
    #                                             p.user_id_bkd = %s
    #                                             and p.target_period_month= %s
    #                                             and p.target_period_year = %s
    #                                             and p.state in ('propose','appeal','rejected_bkd')
    #                                             and p.active
    #                                      '''
    #             cr.execute(query, domain)
    #             total = cr.fetchone() or [0]  # this method returned array
    #             nilai = total[0]
    #     return nilai
    #
    # @api.v7
    # def api_get_total_kegiatan_verifikatur_by_verifikatur(self, cr, uid, ids, context=None):
    #     user_id = len(ids) and ids[0] or False
    #     nilai = 0.0
    #     now = DateTime.today();
    #     last_month = now + DateTime.RelativeDateTime(months=-1)
    #     p_month = str(last_month.month).zfill(2)
    #     p_year = str(last_month.year)
    #     if user_id:
    #         if p_month and p_year:
    #             domain = user_id, p_month, p_year
    #             query = ''' select count(p.id)
    #                                             from project_skp p
    #                                             where
    #                                                 p.user_id_bkd = %s
    #                                                 and p.target_period_month= %s
    #                                                 and p.target_period_year = %s
    #                                                 and p.state in ('evaluated','propose_to_close','propose_to_review')
    #                                                 and p.active
    #                                          '''
    #             cr.execute(query, domain)
    #             total = cr.fetchone() or [0]  # this method returned array
    #             nilai = total[0]
    #     return nilai
    #
    # @api.v7
    # def api_get_total_kegiatan_selesai_by_verifikatur(self, cr, uid, ids, context=None):
    #     user_id = len(ids) and ids[0] or False
    #     nilai = 0.0
    #     now = DateTime.today();
    #     last_month = now + DateTime.RelativeDateTime(months=-1)
    #     p_month = str(last_month.month).zfill(2)
    #     p_year = str(last_month.year)
    #     if user_id:
    #         if p_month and p_year:
    #             domain = user_id, p_month, p_year
    #             query = ''' select count(p.id)
    #                                                 from project_skp p
    #                                                 where
    #                                                     p.user_id_bkd = %s
    #                                                     and p.target_period_month= %s
    #                                                     and p.target_period_year = %s
    #                                                     and p.state = 'done'
    #                                                     and p.active
    #                                              '''
    #             cr.execute(query, domain)
    #             total = cr.fetchone() or [0]  # this method returned array
    #             nilai = total[0]
    #     return nilai
    #
    # @api.v7
    # def api_is_atasan(self, cr, uid, ids, context=None):
    #     user_id = len(ids) and ids[0] or False
    #     is_atasan = False
    #
    #     if user_id:
    #         domain = _ID_GROUP_SKP_ATASAN,user_id
    #         query = ''' select uid from res_groups_users_rel
    #                     where gid = %s and uid = %s
    #                 '''
    #         cr.execute(query, domain)
    #         res_uid = cr.fetchone() or [0]  # this method returned array
    #         is_atasan = (res_uid == user_id)
    #     return is_atasan
# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'API SKP Mobile',
    'version': '1.0',
    'author': 'Darmawan Fatriananda',
    'category': 'SKP',
    'sequence': 90,
    'summary': 'API for Mobile Apps',
    'description': """
SKP
==========================
SKP API
    """,
    'website': 'http://www.-',
    'depends': [
        'df_partner_employee',
        'df_project',
        'df_skp_employee',
        'df_project_multiple_job'
    ],
    'data': [
        'partner_employee_view.xml',
    ],
    'installable': True,
    'active': True,
}

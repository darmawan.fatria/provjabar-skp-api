from openerp import _, api, fields, models
from mx import DateTime


class skp_employee(models.Model):
    _inherit = 'skp.employee'

    def action_get_nilai_prestasi_kerja_api(self, cr, uid,user_id, vals, context=None):
        nilai = 0.0
        if user_id and vals:
            target_period_month = vals.get('target_period_month', False);
            target_period_year = vals.get('target_period_year', 0);

            if target_period_month and target_period_year:
                domain = user_id, target_period_month, target_period_year
                query = '''
                                 select p.nilai_total
                                    from skp_employee p
                                    where p.user_id = %s
                                     and p.target_period_month= %s
                                     and p.target_period_year = %s
                                    limit 1
                                '''
                cr.execute(query, domain)
                nilai_total = cr.fetchone() or [0.0]  # this method returned array
                nilai = nilai_total[0]
        return nilai

    def action_get_detail_nilai_prestasi_kerja_api(self, cr, uid, user_id, vals, context=None):
        nilai_prestasi_kerja = {
            'nilai_skp': 0.0,
            'nilai_tugas_tambahan': 0.0,
            'nilai_perilaku': 0.0
        }
        if user_id and vals:
            target_period_month = vals.get('target_period_month', False);
            target_period_year = vals.get('target_period_year', 0);

            if target_period_month and target_period_year:
                domain = user_id, target_period_month, target_period_year
                query = '''
                                 select p.nilai_skp_percent,p.fn_nilai_tambahan,p.fn_nilai_kreatifitas,p.nilai_perilaku_percent
                                    from skp_employee p
                                    where p.user_id = %s
                                     and p.target_period_month= %s
                                     and p.target_period_year = %s
                                    limit 1
                                '''
                cr.execute(query, domain)
                nilai_prestasi_kerja_arr = cr.fetchone() or [0.0,0.0,0.0,0.0]  # this method returned array
                nilai_prestasi_kerja ={
                    'nilai_skp' : nilai_prestasi_kerja_arr[0],
                    'nilai_tugas_tambahan' : (nilai_prestasi_kerja_arr[1]+nilai_prestasi_kerja_arr[2]),
                    'nilai_perilaku' : nilai_prestasi_kerja_arr[3]
                }
        return nilai_prestasi_kerja

skp_employee()
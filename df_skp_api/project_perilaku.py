from openerp.osv import fields, osv
from openerp import SUPERUSER_ID


class project_perilaku(osv.Model):
    _inherit = 'project.perilaku'

    _columns = {
        'android_api': fields.boolean('Android API'),
    }


    def action_propose_api(self, cr, uid, task_id,vals, context=None):

        print "debug action propose api... set done" #set init
        self.action_target_done(cr,uid,[task_id],context=None);
        print "perilaku param : ",
        print vals
        #then update value
        if task_id:
            satuan_konsumen_code = vals.get('realisasi_satuan_jumlah_konsumen_pelayanan', False)
            print satuan_konsumen_code
            satuan_hitung_pool = self.pool.get('satuan.hitung');
            satuan_hitung_ids = satuan_hitung_pool.search(cr,uid, [('complaint_source', '=', satuan_konsumen_code)], limit=1, context=None)
            print satuan_hitung_ids
            satuan_konsumen_id =False
            if satuan_hitung_ids :
                satuan_konsumen_id = satuan_hitung_ids[0]
            print satuan_konsumen_id
            integritas_hukuman = 'tidak'
            if vals.get('realisasi_integritas_hukuman',False):
                integritas_hukuman='ya'
            vals.update({
                'realisasi_jumlah_konsumen_pelayanan'       : vals.get('realisasi_jumlah_konsumen_pelayanan',False),
                'realisasi_satuan_jumlah_konsumen_pelayanan': satuan_konsumen_id, #Recheck
                'realisasi_jumlah_tidakpuas_pelayanan'      : vals.get('realisasi_jumlah_tidakpuas_pelayanan',False),
                'realisasi_ketepatan_laporan_spj'           : vals.get('realisasi_ketepatan_laporan_spj_id',False), #Recheck
                'realisasi_ketepatan_laporan_ukp4'          : vals.get('realisasi_ketepatan_laporan_ukp4_id',False),#Recheck
                'realisasi_efisiensi_biaya_operasional'     : vals.get('realisasi_efisiensi_biaya_operasional_id',False), #Recheck

                                #'realisasi_apel_pagi'     : vals.get('realisasi_apel_pagi',False),
                                #'realisasi_upacara_hari_besar': vals.get('realisasi_upacara_hari_besar',False),
                                'realisasi_hadir_upacara_hari_besar': vals.get('realisasi_hadir_upacara_hari_besar',False),
                                #'realisasi_jumlah_hari_kerja'     : vals.get('realisasi_jumlah_hari_kerja',False),
                                #'realisasi_jumlah_jam_kerja': vals.get('realisasi_jumlah_jam_kerja',False),
                                'realisasi_hadir_hari_kerja'     : vals.get('realisasi_hadir_hari_kerja',False),
                                'realisasi_hadir_jam_kerja': vals.get('realisasi_hadir_jam_kerja',False),
                                'realisasi_hadir_apel_pagi': vals.get('realisasi_hadir_apel_pagi',False),

                                'realisasi_integritas_presiden': vals.get('realisasi_integritas_presiden',False),
                                'realisasi_integritas_gubernur': vals.get('realisasi_integritas_gubernur',False),
                                'realisasi_integritas_kepalaopd': vals.get('realisasi_integritas_kepalaopd',False),
                                'realisasi_integritas_atasan': vals.get('realisasi_integritas_atasan',False),
                                'realisasi_integritas_es1': vals.get('realisasi_integritas_es1',False),
                                'realisasi_integritas_es2': vals.get('realisasi_integritas_es2',False),
                                'realisasi_integritas_es3': vals.get('realisasi_integritas_es3',False),
                                'realisasi_integritas_es4': vals.get('realisasi_integritas_es4',False),

                                'realisasi_integritas_hukuman': integritas_hukuman,
                                'realisasi_integritas_hukuman_ringan': vals.get('realisasi_integritas_hukuman_ringan',False),
                                'realisasi_integritas_hukuman_sedang': vals.get('realisasi_integritas_hukuman_sedang',False),
                                'realisasi_integritas_hukuman_berat': vals.get('realisasi_integritas_hukuman_berat',False),

                                'realisasi_kerjasama_nasional': vals.get('realisasi_kerjasama_nasional',False),
                                'realisasi_kerjasama_gubernur': vals.get('realisasi_kerjasama_gubernur',False),
                                'realisasi_kerjasama_kepalaopd': vals.get('realisasi_kerjasama_kepalaopd',False),
                                'realisasi_kerjasama_atasan':vals.get('realisasi_kerjasama_atasan',False),
                                'realisasi_kerjasama_rapat_nasional': vals.get('realisasi_kerjasama_rapat_nasional',False),
                                'realisasi_kerjasama_rapat_provinsi': vals.get('realisasi_kerjasama_rapat_provinsi',False),
                                'realisasi_kerjasama_rapat_perangkat_daerah': vals.get('realisasi_kerjasama_rapat_perangkat_daerah',False),
                                'realisasi_kerjasama_rapat_atasan': vals.get('realisasi_kerjasama_rapat_atasan',False),

                                'realisasi_kepemimpinan_nasional': vals.get('realisasi_kepemimpinan_nasional',False),
                                'realisasi_kepemimpinan_gubernur': vals.get('realisasi_kepemimpinan_gubernur',False),
                                'realisasi_kepemimpinan_kepalaopd': vals.get('realisasi_kepemimpinan_kepalaopd',False),
                                'realisasi_kepemimpinan_atasan':  vals.get('realisasi_kepemimpinan_atasan',False),
                                'realisasi_kepemimpinan_narsum_nasional': vals.get('realisasi_kepemimpinan_narsum_nasional',False),
                                'realisasi_kepemimpinan_narsum_provinsi': vals.get('realisasi_kepemimpinan_narsum_provinsi',False),
                                'realisasi_kepemimpinan_narsum_perangkat_daerah': vals.get('realisasi_kepemimpinan_narsum_perangkat_daerah',False),
                                'realisasi_kepemimpinan_narsum_unitkerja': vals.get('realisasi_kepemimpinan_narsum_unitkerja',False),

                'realisasi_integritas_presiden_attach': vals.get('realisasi_integritas_presiden_attach', False),
                'realisasi_integritas_gubernur_attach': vals.get('realisasi_integritas_gubernur_attach', False),
                'realisasi_integritas_kepalaopd_attach': vals.get('realisasi_integritas_kepalaopd_attach', False),
                'realisasi_integritas_atasan_attach': vals.get('realisasi_integritas_atasan_attach', False),
                'realisasi_integritas_es1_attach': vals.get('realisasi_integritas_es1_attach', False),
                'realisasi_integritas_es2_attach': vals.get('realisasi_integritas_es2_attach', False),

                'realisasi_integritas_presiden_date': vals.get('realisasi_integritas_presiden_date', False),
                'realisasi_integritas_gubernur_date': vals.get('realisasi_integritas_gubernur_date', False),
                'realisasi_integritas_kepalaopd_date': vals.get('realisasi_integritas_kepalaopd_date', False),
                'realisasi_integritas_atasan_date': vals.get('realisasi_integritas_atasan_date', False),
                'realisasi_integritas_es1_date': vals.get('realisasi_integritas_es1_date', False),
                'realisasi_integritas_es2_date': vals.get('realisasi_integritas_es2_date', False),

                'realisasi_kerjasama_nasional_attach': vals.get('realisasi_kerjasama_nasional_attach', False),
                'realisasi_kerjasama_gubernur_attach': vals.get('realisasi_kerjasama_gubernur_attach', False),
                'realisasi_kerjasama_kepalaopd_attach': vals.get('realisasi_kerjasama_kepalaopd_attach', False),
                'realisasi_kerjasama_atasan_attach': vals.get('realisasi_kerjasama_atasan_attach', False),
                'realisasi_kerjasama_rapat_nasional_attach': vals.get('realisasi_kerjasama_rapat_nasional_attach', False),
                'realisasi_kerjasama_rapat_provinsi_attach': vals.get('realisasi_kerjasama_rapat_provinsi_attach',False),
                'realisasi_kerjasama_rapat_perangkat_daerah_attach': vals.get('realisasi_kerjasama_rapat_perangkat_daerah_attach', False),
                'realisasi_kerjasama_rapat_atasan_attach': vals.get('realisasi_kerjasama_rapat_atasan_attach', False),

                'realisasi_kerjasama_nasional_date': vals.get('realisasi_kerjasama_nasional_date', False),
                'realisasi_kerjasama_gubernur_date': vals.get('realisasi_kerjasama_gubernur_date', False),
                'realisasi_kerjasama_kepalaopd_date': vals.get('realisasi_kerjasama_kepalaopd_date', False),
                'realisasi_kerjasama_atasan_date': vals.get('realisasi_kerjasama_atasan_date', False),
                'realisasi_kerjasama_rapat_nasional_date': vals.get('realisasi_kerjasama_rapat_nasional_date', False),
                'realisasi_kerjasama_rapat_provinsi_date': vals.get('realisasi_kerjasama_rapat_provinsi_date', False),
                'realisasi_kerjasama_rapat_perangkat_daerah_date': vals.get('realisasi_kerjasama_rapat_perangkat_daerah_date', False),
                'realisasi_kerjasama_rapat_atasan_date': vals.get('realisasi_kerjasama_rapat_atasan_date', False),

                'realisasi_kepemimpinan_nasional_attach': vals.get('realisasi_kepemimpinan_nasional_attach', False),
                'realisasi_kepemimpinan_gubernur_attach': vals.get('realisasi_kepemimpinan_gubernur_attach', False),
                'realisasi_kepemimpinan_kepalaopd_attach': vals.get('realisasi_kepemimpinan_kepalaopd_attach', False),
                'realisasi_kepemimpinan_atasan_attach': vals.get('realisasi_kepemimpinan_atasan_attach', False),
                'realisasi_kepemimpinan_narsum_nasional_attach': vals.get('realisasi_kepemimpinan_narsum_nasional_attach', False),
                'realisasi_kepemimpinan_narsum_provinsi_attach': vals.get('realisasi_kepemimpinan_narsum_provinsi_attach', False),
                'realisasi_kepemimpinan_narsum_perangkat_daerah_attach': vals.get('realisasi_kepemimpinan_narsum_perangkat_daerah_attach', False),
                'realisasi_kepemimpinan_narsum_unitkerja_attach': vals.get('realisasi_kepemimpinan_narsum_unitkerja_attach', False),

                'realisasi_kepemimpinan_nasional_date': vals.get('realisasi_kepemimpinan_nasional_date', False),
                'realisasi_kepemimpinan_gubernur_date': vals.get('realisasi_kepemimpinan_gubernur_date', False),
                'realisasi_kepemimpinan_kepalaopd_date': vals.get('realisasi_kepemimpinan_kepalaopd_date', False),
                'realisasi_kepemimpinan_atasan_date': vals.get('realisasi_kepemimpinan_atasan_date', False),
                'realisasi_kepemimpinan_narsum_nasional_date': vals.get('realisasi_kepemimpinan_narsum_nasional_date',False),
                'realisasi_kepemimpinan_narsum_provinsi_date': vals.get('realisasi_kepemimpinan_narsum_provinsi_date', False),
                'realisasi_kepemimpinan_narsum_perangkat_daerah_date': vals.get('realisasi_kepemimpinan_narsum_perangkat_daerah_date', False),
                'realisasi_kepemimpinan_narsum_unitkerja_date': vals.get('realisasi_kepemimpinan_narsum_unitkerja_date',False),



                'android_api': True,
                'state': 'propose'
            })
            print "updated vals : "
            print vals
            self.write(cr, uid, task_id, vals, context)
            return True
        # end if
        return False;

    def action_evaluated_api(self, cr, uid, task_id, context=None):
        if task_id:
            self.action_evaluated(cr, uid, [task_id], context)
            return True
        return False;

    def action_rejected_api(self, cr, uid, task_id,values, context=None):
        if task_id:
            notes_atasan = values.get('notes_atasan', '');
            vals = {
                'is_suggest': True,
                'state': 'realisasi',
                'android_api': True,
                'notes_atasan':notes_atasan
            }
            self.write(cr, uid, task_id, vals, context)
            return True
        return False;

    def action_review_api(self, cr, uid, task_id, vals, context=None):
        print "perilaku param : ",
        print vals
        if task_id:
            review_notes = vals.get('review_notes', '-');
            satuan_konsumen_code = vals.get('realisasi_satuan_jumlah_konsumen_pelayanan', False)
            print satuan_konsumen_code
            satuan_hitung_pool = self.pool.get('satuan.hitung');
            satuan_hitung_ids = satuan_hitung_pool.search(cr, uid, [('complaint_source', '=', satuan_konsumen_code)],
                                                          limit=1, context=None)
            print satuan_hitung_ids
            satuan_konsumen_id = False
            if satuan_hitung_ids:
                satuan_konsumen_id = satuan_hitung_ids[0]
            print satuan_konsumen_id
            integritas_hukuman = 'tidak'
            if vals.get('realisasi_integritas_hukuman', False):
                integritas_hukuman = 'ya'
            nilai_sebelum_review = vals.get('nilai_sebelum_review', 0);
            if nilai_sebelum_review == 0:
                review_obj = self.browse(cr, uid, task_id, context=context)
                nilai_sebelum_review = review_obj.nilai_akhir

            is_orientasi_pelayanan_review = vals.get('is_orientasi_pelayanan_review', False)
            is_disiplin_review = vals.get('is_disiplin_review', False)
            is_komitmen_review = vals.get('is_komitmen_review', False)
            is_integritas_review = vals.get('is_integritas_review', False)
            is_kerjasama_review = vals.get('is_kerjasama_review', False)
            is_kepemimpinan_review = vals.get('is_kepemimpinan_review', False)

            if is_orientasi_pelayanan_review:
                vals.update({'review_realisasi_jumlah_konsumen_pelayanan': vals.get(
                    'review_realisasi_jumlah_konsumen_pelayanan'),
                    # 'review_realisasi_satuan_jumlah_konsumen_pelayanan': vals.get('review_realisasi_satuan_jumlah_konsumen_pelayanan.id'),
                    'review_realisasi_jumlah_tidakpuas_pelayanan': vals.get(
                        'review_realisasi_jumlah_tidakpuas_pelayanan'),
                    # 'review_realisasi_ketepatan_laporan_spj': vals.get('review_realisasi_ketepatan_laporan_spj.id'),
                    # 'review_realisasi_ketepatan_laporan_ukp4': vals.get('review_realisasi_ketepatan_laporan_ukp4.id'),
                    # 'review_realisasi_efisiensi_biaya_operasional': vals.get('review_realisasi_efisiensi_biaya_operasional.id'),
                })
            if is_disiplin_review:
                vals.update({
                    'review_realisasi_hadir_hari_kerja': vals.get('review_realisasi_hadir_hari_kerja'),
                    'review_realisasi_hadir_jam_kerja': vals.get('review_realisasi_hadir_jam_kerja'),

                })
            if is_komitmen_review:
                vals.update({
                    'review_realisasi_hadir_upacara_hari_besar': vals.get('review_realisasi_hadir_upacara_hari_besar'),
                    'review_realisasi_hadir_apel_pagi': vals.get('review_realisasi_hadir_apel_pagi'),
                })
            if is_integritas_review:
                vals.update({
                    'review_realisasi_integritas_hukuman': integritas_hukuman,
                    'review_realisasi_integritas_hukuman_ringan': vals.get(
                        'review_realisasi_integritas_hukuman_ringan'),
                    'review_realisasi_integritas_hukuman_sedang': vals.get(
                        'review_realisasi_integritas_hukuman_sedang'),
                    'review_realisasi_integritas_hukuman_berat': vals.get('review_realisasi_integritas_hukuman_berat'),
                })
                vals.update({
                    'review_realisasi_integritas_presiden': vals.get('review_realisasi_integritas_presiden'),
                    'review_realisasi_integritas_gubernur': vals.get('review_realisasi_integritas_gubernur'),
                    'review_realisasi_integritas_kepalaopd': vals.get('review_realisasi_integritas_kepalaopd'),
                    'review_realisasi_integritas_atasan': vals.get('review_realisasi_integritas_atasan'),
                    'review_realisasi_integritas_es1': vals.get('review_realisasi_integritas_es1'),
                    'review_realisasi_integritas_es2': vals.get('review_realisasi_integritas_es2'),
                    'review_realisasi_integritas_es3': vals.get('review_realisasi_integritas_es3'),
                    'review_realisasi_integritas_es4': vals.get('review_realisasi_integritas_es4'),
                })
            if is_kerjasama_review:
                vals.update({
                    'review_realisasi_kerjasama_nasional': vals.get('review_realisasi_kerjasama_nasional'),
                    'review_realisasi_kerjasama_gubernur': vals.get('review_realisasi_kerjasama_gubernur'),
                    'review_realisasi_kerjasama_kepalaopd': vals.get('review_realisasi_kerjasama_kepalaopd'),
                    'review_realisasi_kerjasama_atasan': vals.get('review_realisasi_kerjasama_atasan'),
                    'review_realisasi_kerjasama_rapat_nasional': vals.get('review_realisasi_kerjasama_rapat_nasional'),
                    'review_realisasi_kerjasama_rapat_provinsi': vals.get('review_realisasi_kerjasama_rapat_provinsi'),
                    'review_realisasi_kerjasama_rapat_perangkat_daerah': vals.get(
                        'review_realisasi_kerjasama_rapat_perangkat_daerah'),
                    'review_realisasi_kerjasama_rapat_atasan': vals.get('review_realisasi_kerjasama_rapat_atasan'),
                })
            if is_kepemimpinan_review:
                vals.update({
                    'review_realisasi_kepemimpinan_nasional': vals.get('review_realisasi_kepemimpinan_nasional'),
                    'review_realisasi_kepemimpinan_gubernur': vals.get('review_realisasi_kepemimpinan_gubernur'),
                    'review_realisasi_kepemimpinan_kepalaopd': vals.get('review_realisasi_kepemimpinan_kepalaopd'),
                    'review_realisasi_kepemimpinan_atasan': vals.get('review_realisasi_kepemimpinan_atasan'),
                    'review_realisasi_kepemimpinan_narsum_provinsi': vals.get(
                        'review_realisasi_kepemimpinan_narsum_provinsi'),
                    'review_realisasi_kepemimpinan_narsum_perangkat_daerah': vals.get(
                        'review_realisasi_kepemimpinan_narsum_perangkat_daerah'),
                    'review_realisasi_kepemimpinan_narsum_unitkerja': vals.get(
                        'review_realisasi_kepemimpinan_narsum_unitkerja'),
                })

            vals.update({'is_orientasi_pelayanan_review': is_orientasi_pelayanan_review,
                         'is_disiplin_review': is_disiplin_review,
                         'is_komitmen_review': is_komitmen_review,
                         'is_integritas_review': is_integritas_review,
                         'is_kerjasama_review': is_kerjasama_review,
                         'is_kepemimpinan_review': is_kepemimpinan_review,
                         })
            vals.update({
                'is_review': True,
                'review_notes': review_notes,
                'nilai_sebelum_review': nilai_sebelum_review,
                'state': 'propose_to_review',
                'android_api': True
            })
            self.write(cr, uid, task_id, vals, context)
            return True
        # end if
        return False;

project_perilaku()

from openerp.osv import fields, osv
from openerp import SUPERUSER_ID


class project_skp(osv.Model):
    _inherit = 'project.skp'

    _columns = {
        'android_api': fields.boolean('Android API'),
    }

    def action_propose_api(self, cr, uid, task_id,vals, context=None):
        if task_id:
            #self.fill_target_automatically_with_task(cr, uid, ids, context);
            realisasi_jumlah_kuantitas_output = vals.get('realisasi_jumlah_kuantitas_output',0);
            realisasi_angka_kredit = vals.get('realisasi_angka_kredit', 0);
            realisasi_kualitas = vals.get('realisasi_kualitas', 0);
            realisasi_waktu = vals.get('realisasi_waktu', 0);
            realisasi_biaya = vals.get('realisasi_biaya', 0);
            notes = vals.get('notes', '');
            #realisasi_jumlah_kuantitas_output = vals.get('realisasi_satuan_kuantitas_output', False);
            vals.update({
                'realisasi_jumlah_kuantitas_output': realisasi_jumlah_kuantitas_output,
                'realisasi_angka_kredit': realisasi_angka_kredit,
                'realisasi_kualitas': realisasi_kualitas,
                'realisasi_waktu': realisasi_waktu,
                'realisasi_biaya': realisasi_biaya,

                'suggest_jumlah_kuantitas_output': realisasi_jumlah_kuantitas_output,
                'suggest_angka_kredit': realisasi_angka_kredit,
                'suggest_kualitas': realisasi_kualitas,
                'suggest_waktu': realisasi_waktu,
                'suggest_biaya': realisasi_biaya,
                'android_api': True,
                'state': 'propose',
                'notes': notes
            })
            self.write(cr, uid, task_id, vals, context)
            return True
        # end if
        return False;

    def action_evaluated_api(self, cr, uid, task_id, context=None):
        if task_id:
            self.action_evaluated(cr, uid, [task_id], context)
            return True
        return False;
    def action_rejected_api(self, cr, uid, task_id, values,context=None):
        if task_id:
            notes_atasan = values.get('notes_atasan', '');
            vals = {
                'is_suggest' : True,
                'state': 'realisasi',
                'android_api': True,
                'notes_atasan' :notes_atasan
            }
            self.write(cr, uid, task_id, vals, context)
            return True
        return False;

    def action_propose_dpa_review_api(self, cr, uid, task_id, vals, context=None):
        if task_id:
            review_notes = vals.get('review_notes', '-');
            review_realiasasi_biaya = vals.get('review_realiasasi_biaya', 0);
            nilai_sebelum_review = vals.get('nilai_sebelum_review', 0);
            if nilai_sebelum_review == 0 :
                review_obj = self.browse(cr, uid, task_id, context=context)
                nilai_sebelum_review = review_obj.nilai_akhir

            vals.update({'is_review': True,
                         'is_dpa_review': True,
                         'review_notes': review_notes,
                         'review_realiasasi_biaya': review_realiasasi_biaya,
                         'nilai_sebelum_review': nilai_sebelum_review,
                         'state': 'propose_to_review',
                         'android_api': True
                         })
            self.write(cr, uid, task_id, vals, context)
            return True
        # end if
        return False;

    def action_propose_review_by_atasan_api(self, cr, uid, task_id, vals, context=None):
        if task_id:
            review_notes = vals.get('review_notes', '-');
            review_realiasasi_jumlah_kuantitas_output = vals.get('review_realiasasi_jumlah_kuantitas_output', 0);
            review_realiasasi_kualitas = vals.get('review_realiasasi_kualitas', 0);
            review_realiasasi_waktu = vals.get('review_realiasasi_waktu', 0);
            review_realiasasi_angka_kredit = vals.get('review_realiasasi_angka_kredit', 0);
            review_realiasasi_biaya = vals.get('review_realiasasi_biaya', 0);
            nilai_sebelum_review = vals.get('nilai_sebelum_review', 0);
            if nilai_sebelum_review == 0:
                review_obj = self.browse(cr, uid, task_id, context=context)
                nilai_sebelum_review = review_obj.nilai_akhir

            vals.update({
                        'is_review' : True,
                                    'is_dpa_review':False,
                                    'review_notes' : review_notes,
                                    'review_realiasasi_jumlah_kuantitas_output':review_realiasasi_jumlah_kuantitas_output,
                                    'review_realiasasi_kualitas':review_realiasasi_kualitas,
                                    'review_realiasasi_waktu':review_realiasasi_waktu,
                                    'review_realiasasi_biaya':review_realiasasi_biaya,
                                    'review_realiasasi_angka_kredit':review_realiasasi_angka_kredit,
                                    'nilai_sebelum_review':nilai_sebelum_review,
                                    'state':'propose_to_review',
                        'android_api': True
                         })
            self.write(cr, uid, task_id, vals, context)
            return True
        # end if
        return False;
project_skp()
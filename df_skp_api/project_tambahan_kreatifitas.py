from openerp.osv import fields, osv
from openerp import SUPERUSER_ID


class project_tambahan_kreatifitas(osv.Model):
    _inherit = 'project.tambahan.kreatifitas'

    _columns = {
        'android_api': fields.boolean('Android API'),
    }
    def action_propose_api(self, cr, uid, task_id,vals, context=None):
        #skip realisasi value

        #set value
        if task_id:
            realisasi_tugas_tambahan = vals.get('realisasi_tugas_tambahan',False);
            realisasi_uraian_tugas_tambahan = vals.get('realisasi_uraian_tugas_tambahan', False);
            realisasi_rl_opd_tugas_tambahan = vals.get('realisasi_rl_opd_tugas_tambahan', False);
            realisasi_rl_gubernur_tugas_tambahan = vals.get('realisasi_rl_gubernur_tugas_tambahan', False);
            realisasi_rl_presiden_tugas_tambahan = vals.get('realisasi_rl_presiden_tugas_tambahan', False);

            realisasi_nilai_kreatifitas = vals.get('realisasi_nilai_kreatifitas', False);
            realisasi_uraian_kreatifitas = vals.get('realisasi_uraian_kreatifitas', False);
            realisasi_tupoksi_kreatifitas = vals.get('realisasi_tupoksi_kreatifitas', False);
            realisasi_rl_opd_kreatifitas = vals.get('realisasi_rl_opd_kreatifitas', False);
            realisasi_rl_gubernur_kreatifitas = vals.get('realisasi_rl_gubernur_kreatifitas', False);
            realisasi_rl_presiden_kreatifitas = vals.get('realisasi_rl_presiden_kreatifitas', False);
            vals.update({
                'realisasi_tugas_tambahan': realisasi_tugas_tambahan,
                'realisasi_uraian_tugas_tambahan': realisasi_uraian_tugas_tambahan,
                'realisasi_rl_opd_tugas_tambahan': realisasi_rl_opd_tugas_tambahan,
                'realisasi_rl_gubernur_tugas_tambahan': realisasi_rl_gubernur_tugas_tambahan,
                'realisasi_rl_presiden_tugas_tambahan': realisasi_rl_presiden_tugas_tambahan,

                'realisasi_nilai_kreatifitas': realisasi_nilai_kreatifitas,
                'realisasi_uraian_kreatifitas': realisasi_uraian_kreatifitas,
                'realisasi_tupoksi_kreatifitas': realisasi_tupoksi_kreatifitas,
                'realisasi_rl_opd_kreatifitas': realisasi_rl_opd_kreatifitas,
                'realisasi_rl_gubernur_kreatifitas': realisasi_rl_gubernur_kreatifitas,
                'realisasi_rl_presiden_kreatifitas': realisasi_rl_presiden_kreatifitas,
                'android_api' : True,
                'state': 'propose'
            })
            self.write(cr, uid, task_id, vals, context)
            return True
        # end if
        return False;

    def action_evaluated_api(self, cr, uid, task_id, context=None):
        if task_id:
            self.action_evaluated(cr, uid, [task_id], context)
            return True
        return False;

    def action_rejected_api(self, cr, uid, task_id,values, context=None):
        if task_id:
            notes_atasan = values.get('notes_atasan', '');
            vals = {
                'is_suggest': True,
                'state': 'realisasi',
                'android_api': True,
                'notes_atasan': notes_atasan
            }
            self.write(cr, uid, task_id, vals, context)
            return True
        return False;

project_tambahan_kreatifitas()
from openerp import _, api, fields, models
from mx import DateTime

_ID_GROUP_SKP_ATASAN = 23
class res_partner(models.Model):
    _inherit = 'res.partner'

    @api.multi
    def _get_nilai_skp_bulan_lalu(self):
        now = DateTime.today();
        last_month = now + DateTime.RelativeDateTime(months=-2)
        p_month = str(last_month.month).zfill(2)
        p_year = str(last_month.year)
        for obj in self:
            if obj.employee and not obj.is_company:
                domain = obj.id, p_month, p_year
                query = '''
                         select p.nilai_skp_percent
                            from skp_employee p
                            where p.employee_id = %s
                             and p.target_period_month= %s
                             and p.target_period_year = %s
                            limit 1
                        '''
                self.env.cr.execute(query, domain)
                nilai_skp = self.env.cr.fetchone() or [0.0] #this method returned array
                obj.nilai_skp_bulan_1 = nilai_skp[0]
            else :
                obj.nilai_skp_bulan_1 = 0

    @api.multi
    def _get_nilai_perilaku_bulan_lalu(self):
        now = DateTime.today();
        last_month = now + DateTime.RelativeDateTime(months=-2)
        p_month = str(last_month.month).zfill(2)
        p_year = str(last_month.year)
        for obj in self:
            if obj.employee and not obj.is_company:
                domain = obj.id, p_month, p_year
                query = '''
                             select p.nilai_perilaku_percent
                                from skp_employee p
                                where p.employee_id = %s
                                 and p.target_period_month= %s
                                 and p.target_period_year = %s
                                limit 1
                            '''
                self.env.cr.execute(query, domain)
                nilai_perilaku_percent = self.env.cr.fetchone() or [0.0]  # this method returned array
                obj.nilai_perilaku_bulan_1 = nilai_perilaku_percent[0]
            else :
                obj.nilai_perilaku_bulan_1 = 0

    @api.multi
    def _get_nilai_tb_kreatifitas_bulan_lalu(self):
        now = DateTime.today();
        last_month = now + DateTime.RelativeDateTime(months=-2)
        p_month = str(last_month.month).zfill(2)
        p_year = str(last_month.year)
        for obj in self:
            if obj.employee and not obj.is_company:
                domain = obj.id, p_month, p_year
                query = '''
                                 select (p.fn_nilai_tambahan + p.fn_nilai_kreatifitas)
                                    from skp_employee p
                                    where p.employee_id = %s
                                     and p.target_period_month= %s
                                     and p.target_period_year = %s
                                    limit 1
                                '''
                self.env.cr.execute(query, domain)
                nilai_tambahan_kreatifitas = self.env.cr.fetchone() or [0.0]  # this method returned array
                obj.nilai_tambahan_kreatifitas_bulan_1 = nilai_tambahan_kreatifitas[0]
            else :
                obj.nilai_tambahan_kreatifitas_bulan_1 =0

    @api.multi
    def _get_nilai_prestasi_kerja_bulan_1(self):
        now = DateTime.today();
        last_month = now + DateTime.RelativeDateTime(months=-2)
        p_month = str(last_month.month).zfill(2)
        p_year = str(last_month.year)
        for obj in self:
            if obj.employee and not obj.is_company:
                domain = obj.id, p_month, p_year
                query = '''
                             select p.nilai_total
                                from skp_employee p
                                where p.employee_id = %s
                                 and p.target_period_month= %s
                                 and p.target_period_year = %s
                                limit 1
                            '''
                self.env.cr.execute(query, domain)
                nilai_total = self.env.cr.fetchone() or [0.0]  # this method returned array
                obj.nilai_prestasi_kerja_bulan_1 = nilai_total[0]
            else :
                obj.nilai_prestasi_kerja_bulan_1 = 0

    @api.multi
    def _get_nilai_prestasi_kerja_bulan_2(self):
        now = DateTime.today();
        last_month = now + DateTime.RelativeDateTime(months=-3)
        p_month = str(last_month.month).zfill(2)
        p_year = str(last_month.year)
        for obj in self:
            if obj.employee and not obj.is_company:
                domain = obj.id, p_month, p_year
                query = '''
                                 select p.nilai_total
                                    from skp_employee p
                                    where p.employee_id = %s
                                     and p.target_period_month= %s
                                     and p.target_period_year = %s
                                    limit 1
                                '''
                self.env.cr.execute(query, domain)
                nilai_total = self.env.cr.fetchone() or [0.0]  # this method returned array
                obj.nilai_prestasi_kerja_bulan_2 = nilai_total[0]
            else :
                obj.nilai_prestasi_kerja_bulan_2 = 0

    @api.multi
    def _get_nilai_skp_tahun_1(self):
        now = DateTime.today();
        last_month = now + DateTime.RelativeDateTime(years=-1)
        p_year = str(last_month.year)
        for obj in self:
            if obj.employee and not obj.is_company:
                domain = obj.id, p_year
                query = '''
                             select p.nilai_skp_percent
                                from skp_employee_yearly p
                                where p.employee_id = %s
                                 and p.target_period_year = %s
                                limit 1
                            '''
                self.env.cr.execute(query, domain)
                nilai_skp_percent = self.env.cr.fetchone() or [0.0]  # this method returned array
                obj.nilai_skp_tahun_1 = nilai_skp_percent[0]
            else :
                obj.nilai_skp_tahun_1 = 0

    @api.multi
    def _get_nilai_tambahan_kreatifitas_tahun_1(self):
        now = DateTime.today();
        last_month = now + DateTime.RelativeDateTime(years=-1)
        p_year = str(last_month.year)
        for obj in self:
            if obj.employee and not obj.is_company:
                domain = obj.id, p_year
                query = '''
                                 select (p.fn_nilai_tambahan + fn_nilai_kreatifitas)
                                    from skp_employee_yearly p
                                    where p.employee_id = %s
                                     and p.target_period_year = %s
                                    limit 1
                                '''
                self.env.cr.execute(query, domain)
                nilai_tambahan_kreatifitas = self.env.cr.fetchone() or [0.0]  # this method returned array
                obj.nilai_tambahan_kreatifitas_tahun_1 = nilai_tambahan_kreatifitas[0]
            else :
                obj.nilai_tambahan_kreatifitas_tahun_1 = 0

    @api.multi
    def _get_nilai_perilaku_tahun_1(self):
        now = DateTime.today();
        last_month = now + DateTime.RelativeDateTime(years=-1)
        p_year = str(last_month.year)
        for obj in self:
            if obj.employee and not obj.is_company:
                domain = obj.id, p_year
                query = '''
                                 select p.nilai_perilaku_percent
                                    from skp_employee_yearly p
                                    where p.employee_id = %s
                                     and p.target_period_year = %s
                                    limit 1
                                '''
                self.env.cr.execute(query, domain)
                nilai_perilaku_percent = self.env.cr.fetchone() or [0.0]  # this method returned array
                obj.nilai_perilaku_tahun_1 = nilai_perilaku_percent[0]
            else :
                obj.nilai_perilaku_tahun_1 = 0

    @api.multi
    def _get_total_kegiatan_pegawai(self):
        now = DateTime.today();
        last_month = now + DateTime.RelativeDateTime(months=-1)
        p_month = str(last_month.month).zfill(2)
        p_year = str(last_month.year)
        for obj in self:
            if obj.user_id and obj.employee and not obj.is_company:
                domain = obj.user_id.id, p_month,p_year
                query = ''' select count(p.id)
                            from project_skp p
                            where
                                p.user_id_atasan = %s
                                and p.target_period_month= %s
                                and p.target_period_year = %s
                                and p.state in ('realisasi','rejected_manager')
                                and p.active
                         '''
                self.env.cr.execute(query, domain)
                total = self.env.cr.fetchone() or [0]  # this method returned array
                obj.total_kegiatan_pegawai = total[0]
            else :
                obj.total_kegiatan_pegawai =0

    @api.multi
    def _get_total_kegiatan_atasan(self):
        now = DateTime.today();
        last_month = now + DateTime.RelativeDateTime(months=-1)
        p_month = str(last_month.month).zfill(2)
        p_year = str(last_month.year)
        for obj in self:
            if obj.user_id and obj.employee and not obj.is_company:
                domain = obj.user_id.id, p_month, p_year
                query = ''' select count(p.id)
                                from project_skp p
                                where
                                    p.user_id_atasan = %s
                                    and p.target_period_month= %s
                                    and p.target_period_year = %s
                                    and p.state in ('propose','appeal','rejected_bkd')
                                    and p.active
                             '''
                self.env.cr.execute(query, domain)
                total = self.env.cr.fetchone() or [0]  # this method returned array
                obj.total_kegiatan_atasan = total[0]
            else :
                obj.total_kegiatan_atasan = 0

    @api.multi
    def _get_total_kegiatan_verifikatur(self):
        now = DateTime.today();
        last_month = now + DateTime.RelativeDateTime(months=-1)
        p_month = str(last_month.month).zfill(2)
        p_year = str(last_month.year)
        for obj in self:
            if obj.user_id:
                domain = obj.user_id.id, p_month, p_year
                query = ''' select count(p.id)
                                    from project_skp p
                                    where
                                        p.user_id_atasan = %s
                                        and p.target_period_month= %s
                                        and p.target_period_year = %s
                                        and p.state in ('evaluated','propose_to_close','propose_to_review')
                                        and p.active
                                 '''
                self.env.cr.execute(query, domain)
                total = self.env.cr.fetchone() or [0]  # this method returned array
                obj.total_kegiatan_verifikatur = total[0]
            else :
                obj.total_kegiatan_verifikatur =0

    @api.multi
    def _get_total_kegiatan_selesai(self):
        now = DateTime.today();
        last_month = now + DateTime.RelativeDateTime(months=-1)
        p_month = str(last_month.month).zfill(2)
        p_year = str(last_month.year)
        for obj in self:
            if obj.user_id:
                domain = obj.user_id.id, p_month, p_year
                query = ''' select count(p.id)
                                        from project_skp p
                                        where
                                            p.user_id_atasan = %s
                                            and p.target_period_month= %s
                                            and p.target_period_year = %s
                                            and p.state = 'done'
                                            and p.active
                                     '''
                self.env.cr.execute(query, domain)
                total = self.env.cr.fetchone() or [0]  # this method returned array
                obj.total_kegiatan_selesai = total[0]
            else :
                obj.total_kegiatan_selesai =0

    @api.multi
    def _get_total_kegiatan_pegawai_by_verifikatur(self):
        now = DateTime.today();
        last_month = now + DateTime.RelativeDateTime(months=-1)
        p_month = str(last_month.month).zfill(2)
        p_year = str(last_month.year)
        for obj in self:
            if obj.user_id and not obj.employee and not obj.is_company:
                domain = obj.user_id.id, p_month, p_year
                query = ''' select count(p.id)
                                from project_skp p
                                where
                                    p.user_id_bkd = %s
                                    and p.target_period_month= %s
                                    and p.target_period_year = %s
                                    and p.state in ('realisasi','rejected_manager')
                                    and p.active
                             '''
                self.env.cr.execute(query, domain)
                total = self.env.cr.fetchone() or [0]  # this method returned array
                obj.total_kegiatan_pegawai_verifikatur = total[0]
            else :
                obj.total_kegiatan_pegawai_verifikatur = 0

    @api.multi
    def _get_total_kegiatan_atasan_by_verifikatur(self):
        now = DateTime.today();
        last_month = now + DateTime.RelativeDateTime(months=-1)
        p_month = str(last_month.month).zfill(2)
        p_year = str(last_month.year)
        for obj in self:
            if obj.user_id and not obj.employee and not obj.is_company:
                domain = obj.user_id.id, p_month, p_year
                query = ''' select count(p.id)
                                    from project_skp p
                                    where
                                        p.user_id_bkd = %s
                                        and p.target_period_month= %s
                                        and p.target_period_year = %s
                                        and p.state in ('propose','appeal','rejected_bkd')
                                        and p.active
                                 '''
                self.env.cr.execute(query, domain)
                total = self.env.cr.fetchone() or [0]  # this method returned array
                obj.total_kegiatan_atasan_verifikatur = total[0]
            else :
                obj.total_kegiatan_atasan_verifikatur

    @api.multi
    def _get_total_kegiatan_verifikatur_by_verifikatur(self):
        now = DateTime.today();
        last_month = now + DateTime.RelativeDateTime(months=-1)
        p_month = str(last_month.month).zfill(2)
        p_year = str(last_month.year)
        for obj in self:
            if obj.user_id and not obj.employee and not obj.is_company:
                domain = obj.user_id.id, p_month, p_year
                query = ''' select count(p.id)
                                        from project_skp p
                                        where
                                            p.user_id_bkd = %s
                                            and p.target_period_month= %s
                                            and p.target_period_year = %s
                                            and p.state in ('evaluated','propose_to_close','propose_to_review')
                                            and p.active
                                     '''
                self.env.cr.execute(query, domain)
                total = self.env.cr.fetchone() or [0]  # this method returned array
                obj.total_kegiatan_verifikatur_verifikatur = total[0]
            else :
                obj.total_kegiatan_verifikatur_verifikatur = 0

    @api.multi
    def _get_total_kegiatan_selesai_by_verifikatur(self):
        now = DateTime.today();
        last_month = now + DateTime.RelativeDateTime(months=-1)
        p_month = str(last_month.month).zfill(2)
        p_year = str(last_month.year)
        for obj in self:
            if obj.user_id and not obj.employee and not obj.is_company:
                domain = obj.user_id.id, p_month, p_year
                query = ''' select count(p.id)
                                            from project_skp p
                                            where
                                                p.user_id_bkd = %s
                                                and p.target_period_month= %s
                                                and p.target_period_year = %s
                                                and p.state = 'done'
                                                and p.active
                                         '''
                self.env.cr.execute(query, domain)
                total = self.env.cr.fetchone() or [0]  # this method returned array
                obj.total_kegiatan_selesai_verifikatur = total[0]
            else :
                obj.total_kegiatan_selesai_verifikatur =0

    nilai_skp_bulan_1 = fields.Float(string="Nilai SKP Bulan Lalu", compute='_get_nilai_skp_bulan_lalu', store=False)
    nilai_tambahan_kreatifitas_bulan_1 = fields.Float(string="Nilai Tb & Kreatifitas Bulan Lalu", compute='_get_nilai_tb_kreatifitas_bulan_lalu', store=False)
    nilai_perilaku_bulan_1 = fields.Float(string="Nilai Perilaku Bulan Lalu", compute='_get_nilai_perilaku_bulan_lalu', store=False)
    nilai_prestasi_kerja_bulan_1 = fields.Float(string="Nilai Prestasi Kerja 1 Bulan Lalu", compute='_get_nilai_prestasi_kerja_bulan_1',store=False)
    nilai_prestasi_kerja_bulan_2 = fields.Float(string="Nilai Prestasi Kerja 2 Bulan Lalu",compute='_get_nilai_prestasi_kerja_bulan_2', store=False)
    nilai_skp_tahun_1 = fields.Float(string="Nilai SKP Tahun Lalu", compute='_get_nilai_skp_tahun_1', store=False)
    nilai_tambahan_kreatifitas_tahun_1 = fields.Float(string="Nilai TB & Kreatifitas Tahun Lalu", compute='_get_nilai_tambahan_kreatifitas_tahun_1', store=False)
    nilai_perilaku_tahun_1 = fields.Float(string="Nilai Perilaku Tahun Lalu", compute='_get_nilai_perilaku_tahun_1', store=False)
    total_kegiatan_pegawai = fields.Integer(string="Kegiatan Pegawai", compute='_get_total_kegiatan_pegawai',store=False)
    total_kegiatan_atasan = fields.Integer(string="Kegiatan Atasan", compute='_get_total_kegiatan_atasan', store=False)
    total_kegiatan_verifikatur = fields.Integer(string="Kegiatan Verifikatur", compute='_get_total_kegiatan_verifikatur', store=False)
    total_kegiatan_selesai = fields.Integer(string="Kegiatan Selesai", compute='_get_total_kegiatan_selesai', store=False)
    total_kegiatan_pegawai_verifikatur = fields.Integer(string="Kegiatan Pegawai", compute='_get_total_kegiatan_pegawai_by_verifikatur',
                                            store=False)
    total_kegiatan_atasan_verifikatur = fields.Integer(string="Kegiatan Atasan", compute='_get_total_kegiatan_atasan_by_verifikatur', store=False)
    total_kegiatan_verifikatur_verifikatur = fields.Integer(string="Kegiatan Verifikatur",
                                                compute='_get_total_kegiatan_verifikatur_by_verifikatur', store=False)
    total_kegiatan_selesai_verifikatur = fields.Integer(string="Kegiatan Selesai", compute='_get_total_kegiatan_selesai_by_verifikatur',
                                            store=False)

    @api.v7
    def api_get_nilai_skp_bulan_lalu(self, cr, uid, ids, context=None):
        partner_id = len(ids) and ids[0] or False
        nilai = 0.0
        now = DateTime.today();
        last_month = now + DateTime.RelativeDateTime(months=-2)
        p_month = str(last_month.month).zfill(2)
        p_year = str(last_month.year)
        if partner_id:
            if p_month and p_year:
                domain = partner_id, p_month, p_year
                query = '''
                             select p.nilai_skp_percent
                                from skp_employee p
                                where p.employee_id = %s
                                 and p.target_period_month= %s
                                 and p.target_period_year = %s
                                limit 1
                            '''
                cr.execute(query, domain)
                nilai_skp = cr.fetchone() or [0.0]  # this method returned array
                nilai = nilai_skp[0]
        return nilai;

    @api.v7
    def api_get_nilai_perilaku_bulan_lalu(self, cr, uid, ids, context=None):
        partner_id = len(ids) and ids[0] or False
        nilai = 0.0
        now = DateTime.today();
        last_month = now + DateTime.RelativeDateTime(months=-2)
        p_month = str(last_month.month).zfill(2)
        p_year = str(last_month.year)
        if partner_id:
            if p_month and p_year:
                domain = partner_id, p_month, p_year
                query = '''
                             select p.nilai_perilaku_percent
                                from skp_employee p
                                where p.employee_id = %s
                                 and p.target_period_month= %s
                                 and p.target_period_year = %s
                                limit 1
                            '''
                cr.execute(query, domain)
                nilai_perilaku_percent = cr.fetchone() or [0.0]  # this method returned array
                nilai = nilai_perilaku_percent[0]
        return nilai

    @api.v7
    def api_get_nilai_tb_kreatifitas_bulan_lalu(self, cr, uid, ids, context=None):
        partner_id = len(ids) and ids[0] or False
        nilai = 0.0
        now = DateTime.today();
        last_month = now + DateTime.RelativeDateTime(months=-2)
        p_month = str(last_month.month).zfill(2)
        p_year = str(last_month.year)
        if partner_id:
            if p_month and p_year:
                domain = partner_id, p_month, p_year
                query = '''
                                     select (p.fn_nilai_tambahan + p.fn_nilai_kreatifitas)
                                        from skp_employee p
                                        where p.employee_id = %s
                                         and p.target_period_month= %s
                                         and p.target_period_year = %s
                                        limit 1
                                    '''
                cr.execute(query, domain)
                nilai_tambahan_kreatifitas = cr.fetchone() or [0.0]  # this method returned array
                nilai = nilai_tambahan_kreatifitas[0]
        return nilai

    @api.v7
    def api_get_nilai_prestasi_kerja_bulan_1(self, cr, uid, ids, context=None):
        partner_id = len(ids) and ids[0] or False
        nilai = 0.0
        now = DateTime.today();
        last_month = now + DateTime.RelativeDateTime(months=-2)
        p_month = str(last_month.month).zfill(2)
        p_year = str(last_month.year)
        if partner_id:
            if p_month and p_year:
                domain = partner_id, p_month, p_year
                query = '''
                                 select p.nilai_total
                                    from skp_employee p
                                    where p.employee_id = %s
                                     and p.target_period_month= %s
                                     and p.target_period_year = %s
                                    limit 1
                                '''
                cr.execute(query, domain)
                nilai_total = cr.fetchone() or [0.0]  # this method returned array
                nilai = nilai_total[0]
        return nilai

    @api.v7
    def api_get_nilai_prestasi_kerja_bulan_2(self, cr, uid, ids, context=None):
        partner_id = len(ids) and ids[0] or False
        nilai = 0.0
        now = DateTime.today();
        last_month = now + DateTime.RelativeDateTime(months=-3)
        p_month = str(last_month.month).zfill(2)
        p_year = str(last_month.year)
        if partner_id:
            if p_month and p_year:
                domain = partner_id, p_month, p_year
                query = '''
                                     select p.nilai_total
                                        from skp_employee p
                                        where p.employee_id = %s
                                         and p.target_period_month= %s
                                         and p.target_period_year = %s
                                        limit 1
                                    '''
                cr.execute(query, domain)
                nilai_total = cr.fetchone() or [0.0]  # this method returned array
                nilai = nilai_total[0]
        return nilai

    @api.v7
    def api_get_nilai_skp_tahun_1(self, cr, uid, ids, context=None):
        partner_id = len(ids) and ids[0] or False
        nilai = 0.0
        now = DateTime.today();
        last_month = now + DateTime.RelativeDateTime(years=-1)
        p_year = str(last_month.year)
        if partner_id:
            if p_year:
                domain = partner_id, p_year
                query = '''
                                 select p.nilai_skp_percent
                                    from skp_employee_yearly p
                                    where p.employee_id = %s
                                     and p.target_period_year = %s
                                    limit 1
                                '''
                cr.execute(query, domain)
                nilai_skp_percent = cr.fetchone() or [0.0]  # this method returned array
                nilai = nilai_skp_percent[0]
        return nilai

    @api.v7
    def api_get_nilai_tambahan_kreatifitas_tahun_1(self, cr, uid, ids, context=None):
        partner_id = len(ids) and ids[0] or False
        nilai = 0.0
        now = DateTime.today();
        last_month = now + DateTime.RelativeDateTime(years=-1)
        p_year = str(last_month.year)
        if partner_id:
            if p_year:
                domain = partner_id, p_year
                query = '''
                                     select (p.fn_nilai_tambahan + fn_nilai_kreatifitas)
                                        from skp_employee_yearly p
                                        where p.employee_id = %s
                                         and p.target_period_year = %s
                                        limit 1
                                    '''
                cr.execute(query, domain)
                nilai_tambahan_kreatifitas = cr.fetchone() or [0.0]  # this method returned array
                nilai = nilai_tambahan_kreatifitas[0]
        return nilai

    @api.v7
    def api_get_nilai_perilaku_tahun_1(self, cr, uid, ids, context=None):
        partner_id = len(ids) and ids[0] or False
        nilai = 0.0
        now = DateTime.today();
        last_month = now + DateTime.RelativeDateTime(years=-1)
        p_year = str(last_month.year)
        if partner_id:
            if p_year:
                domain = partner_id, p_year
                query = '''
                                     select p.nilai_perilaku_percent
                                        from skp_employee_yearly p
                                        where p.employee_id = %s
                                         and p.target_period_year = %s
                                        limit 1
                                    '''
                cr.execute(query, domain)
                nilai_perilaku_percent = cr.fetchone() or [0.0]  # this method returned array
                nilai = nilai_perilaku_percent[0]
        return nilai

    @api.v7
    def api_is_atasan(self, cr, uid, ids, context=None):
        user_id = uid
        is_atasan = False
        if user_id:
            domain = _ID_GROUP_SKP_ATASAN, user_id
            query = ''' select uid from res_groups_users_rel
                            where gid = %s and uid = %s
                        '''
            cr.execute(query, domain)
            res_uid = cr.fetchone() or False  # this method returned array
            if res_uid :
                if len(res_uid) == 1 :
                    is_atasan = True
        return is_atasan









